﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace Encryption_Decryption_Program
{
    public partial class Form1 : Form
    {
        byte[] Key;
        byte[] IV;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenKey_Click(object sender, EventArgs e)
        {
            SymmetricAlgorithm sa = new RijndaelManaged();
            sa.GenerateKey();
            Key = sa.Key;
            IV = sa.IV;

            txtKey.Text = BitConverter.ToString(Key);
            btnEncrypt.Enabled = true;
            txtPlain.Enabled = true;
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            SymmetricAlgorithm sa = new RijndaelManaged();
            sa.Key = Key;
            sa.IV = IV;

            ICryptoTransform cryptTransform = sa.CreateEncryptor();
            byte[] plainText = Encoding.UTF8.GetBytes(txtPlain.Text);
            byte[] cipherText = cryptTransform.TransformFinalBlock (plainText, 0, plainText.Length);

            txtCipher.Text = Convert.ToBase64String(cipherText);
            btnDecrypt.Enabled = true;
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            SymmetricAlgorithm sa = new RijndaelManaged();
            sa.Key = Key;
            sa.IV = IV;

            ICryptoTransform cryptTransform = sa.CreateDecryptor();
            byte[] cipherText = Convert.FromBase64String(txtCipher.Text);
            byte[] plainText = cryptTransform.TransformFinalBlock(cipherText, 0, cipherText.Length);

            txtOrg.Text = Encoding.UTF8.GetString(plainText);
        }
    }
}
