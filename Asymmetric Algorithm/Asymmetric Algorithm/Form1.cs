﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace Asymmetric_Algorithm
{
    public partial class Form1 : Form
    {
        String publickey;
        String privatekey;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenKey_Click(object sender, EventArgs e)
        {
            RSACryptoServiceProvider sa = new RSACryptoServiceProvider(2048);
            publickey = sa.ToXmlString(false);
            privatekey = sa.ToXmlString(true);

            txtPublic.Text = "Public Key: " + publickey;
            txtPrivate.Text = "Private Key: " + privatekey;

            btnEncrypt.Enabled = true;
            txtPlain.Enabled = true;
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            byte[] plainText = Encoding.UTF8.GetBytes(txtPlain.Text);

            RSACryptoServiceProvider sa = new RSACryptoServiceProvider();
            sa.FromXmlString(publickey);
            byte[] cipherText = sa.Encrypt(plainText, false);

            txtCipher.Text = Convert.ToBase64String(cipherText);
            btnDecrypt.Enabled = true;
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            byte[] cipherText = Convert.FromBase64String(txtCipher.Text);

            RSACryptoServiceProvider sa = new RSACryptoServiceProvider();
            sa.FromXmlString(privatekey);
            byte[] plainText = sa.Decrypt(cipherText, false);

            txtOrg.Text = Encoding.UTF8.GetString(plainText);
        }
    }
}
